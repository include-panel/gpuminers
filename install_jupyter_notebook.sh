#!/bin/sh
apt update;apt -y install python3-pip python3-dev

pip3 install --upgrade pip
pip3 install virtualenv

mkdir ~/my_project_dir
cd ~/my_project_dir

virtualenv my_project_env

source my_project_env/bin/activate

pip install jupyter

jupyter notebook --allow-root

