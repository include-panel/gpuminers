#!/bin/sh
apt update;apt -y install cmake make gcc bzip2 binutils build-essential

wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/badvpn/badvpn-1.999.128.tar.bz2

tar xf badvpn-1.999.128.tar.bz2

mkdir badvpn-build

cd badvpn-build

cmake ~/badvpn-1.999.128 -DBUILD_NOTHING_BY_DEFAULT=1 -DBUILD_UDPGW=1

make install

badvpn-udpgw --listen-addr 0.0.0.0:7300 > /dev/null &

cd
