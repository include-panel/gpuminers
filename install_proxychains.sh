#!/bin/sh
#apt update
apt -y install proxychains
rm /etc/proxychains.conf
touch /etc/proxychains.conf
echo > /etc/proxychains.conf <<EOL
proxy_dns
tcp_read_time_out 15000
tcp_connect_time_out 8000
[ProxyList]
socks5 127.0.0.1 1338
EOL
sed -i "s/PROXYRESOLV_DNS:-4.2.2.2/PROXYRESOLV_DNS:-8.8.8.8/" /usr/lib/proxychains3/proxyresolv

